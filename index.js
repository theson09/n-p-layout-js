/*
Tính tiền lương nhân viên                

Lương của nhân viên 1 ngày là 100.000
Số ngày làm việc của nhân viên là 30 ngày

Bước 1: Tạo biến luong1Ngay, soNgayLam
Bước 2: Gán giá trị cho luong1Ngay. soNgayLam, luong
Bước 3: Tính tổng lương với số tiền và ngày làm của nhân viên
Bước 4: In kết quả theo biểu mẫu ra console

Kết quả số tiền của nhân viên nhận được 


*/ 
function tinhluong(){
    var luong1Ngay= document.getElementById("txt-luong-mot-ngay").value *1;
    var soNgayLam= document.getElementById("so-ngay-lam").value *1;
    var luong = luong1Ngay * soNgayLam;
    document.getElementById("result").innerHTML=`Tiền lương ${luong}`;
}

// Bài 2
/*
Tính giá trị trung bình

Giá trị 5 số thực: 10, 20, 30, 40, 50

Bước 1: Tạo biến numb1, numb2, numb3, numb4, numb5
Bước 2: Gán giá trị cho numb1, numb2, numb3, numb4, numb5
Bước 3: Tính giá trị của tổng 5 số người dùng nhập chia cho 5
Bước 4: In kết quả ra console

Kết quả giaTriTrungBinh 
*/ 
function tinhTrungBinh() {
    var num1=document.getElementById("txt-num-1").value *1;
    var num2=document.getElementById("txt-num-2").value *1;
    var num3=document.getElementById("txt-num-3").value *1;
    var num4=document.getElementById("txt-num-4").value *1;
    var num5=document.getElementById("txt-num-5").value *1;

    var TrungBinh = (num1 + num2 + num3 + num4 + num5) / 5;
   
    document.getElementById("result-2").innerHTML=`Giá trị là ${TrungBinh}`;  
}
/*
Quy đổi tiền

Giá USD hiện nay = 23.500VND

Bước 1: Tạo biến giaVND, soluongUSD, giaUSD
Bước 2: Gán giá trị cho giaVND, soluongUSD, giaUSD
Bước 3: Quy đổi số lượng USD sang VND 
Bước 4: In kết quả theo biểu mẫu ra console

Kết quả USD người dùng nhập đổi sang VND
*/
// bài 3
function TinhTienVND() {
  var giaVND = 23500;
  var soluongUSD = document.getElementById("txt-USD").value *1;
  
  var QuyDoi = giaVND * soluongUSD;

document.getElementById("result-3").innerHTML = `Giá tiền là ${QuyDoi}`;
}
/*
Tính diện tích, chu vi hình chữ nhật 

Cho chiều dài là 5
Cho chiều rộng là 3

Bước 1: Tạo biến chieuDai, chieuRong, chuVi, dienTich
Bước 2: Gán giá trị cho chieuDai, chieuRong
Bước 3: Tính công thức diện tích và chu vi
Bước 4: In 2 kết quả (chu vi và diện tích) ra console

Kết quả chuvi dienTich (chu vi và diện tích)
//bài 4
*/ 
function dienTichChuVi(){
    var chieuDai = document.getElementById("txt-chieu-dai").value*1;
    var chieuRong = document.getElementById("txt-chieu-rong").value*1;
    
    var chuVi = (chieuDai + chieuRong) * 2;
    var dienTich = chieuDai * chieuRong;
    
    document.getElementById("result-4").innerHTML=`Chu vi là ${chuVi}, Diện tích là ${dienTich}`
    }
    
    /*
    Tính tổng 2 ký số
    
    Số nguyên n có 2 ký số (50)
    5 + 0 = 50
    
    Bước 1: Tạo biến n, hangChuc, donVi, tong2KySo
    Bước 2: Gán giá trị cho n
    Bước 3: Tách số hàng chục theo công thức hangChuc = Math.floor (n/10)
    Bước 4: Tác số hàng đơn vị theo công thức donVi = Math.floor (n % 10);
    Bước 5: In kết quả tong2KySo ra console
    
    Kết quả tong2KySo (tổng 2 ký số)
    // bài 5
    */
    function tinhTong(){
    var n=document.getElementById("txt-hai-chu-so").value*1;
    var hangChuc = Math.floor (n / 10);
    var donVi = Math.floor (n % 10);
    var tong2KySo = hangChuc + donVi;
    document.getElementById("result-5").innerHTML=`Kết quả là ${tong2KySo}`
    
    }